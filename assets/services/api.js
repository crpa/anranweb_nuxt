import request from "../utils/request";
const qs = require("qs");
const prefix = "/php-phalapi/public";

export const getList = params => {
  return request({
    url: `${prefix}/?service=App.Web_Article.SelectArticle`,
    method: "POST",
    data: qs.stringify(params)
  });
};

export const getInfo = params => {
  return request({
    url: `${prefix}/?service=App.Web_Article.SelectArticleInfo`,
    method: "POST",
    data: qs.stringify({ id: params })
  });
};

// 查询置顶文章详情
export const getIsTopInfo = () => {
  return request({
    url: `${prefix}/?service=App.Web_Article.IsTopArticleInfo`,
    method: "POST",
    data: ""
  });
};

// 新增留言
export const addMsg = d => {
  return request({
    url: `${prefix}/?service=App.Web_Msg.AddMsg`,
    method: "POST",
    data: qs.stringify(d)
  });
};

// 生成二维码
export const creatQrCode = d => {
  return request({
    url: `${prefix}/?service=App.Examples_QrCode.Png`,
    method: "POST",
    data: qs.stringify(d)
  });
};
