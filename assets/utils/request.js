import axios from "axios";

// 请求拦截器
axios.interceptors.request.use(
  request => {
    return request;
  },
  error => {
    return Promise.reject(error);
  }
);

export default async (options = { method: "GET" }) => {
  let isdata = true;
  if (
    options.method.toUpperCase() !== "POST" &&
    options.method.toUpperCase() !== "PUT" &&
    options.method.toUpperCase() !== "PATCH"
  ) {
    isdata = false;
  }
  const res = await axios({
    method: options.method,
    url: options.url,
    data: isdata ? options.data : null,
    params: !isdata ? options.data : null
  });
  if (res.status >= 200 && res.status < 300) {
    // 浏览器环境弹出报错信息
    if (typeof window !== "undefined" && res.data.ret !== 200) {
      alert(res.data.msg);
    }
    return res.data.data;
  } else {
    alert("请求错误");
  }
};
