# nuxtBootstrap

> 安然网--前端代码
> [在线预览](http://www.anranweb.cn/ "安然网")

## Build Setup

``` bash
# 安装依赖包
$ npm install --registry=https://registry.npm.taobao.org

# 运行开发模式 
$ npm run dev

# 发布到服务器
$ npm run build
$ npm start

# 打包成静态文件发布
$ npm run generate
```

## 项目目录
```
├── assets                     // 静态资源
│   ├── css                       // 样式文件
│   ├── img                       // 图片资源
│   ├── services                  // api请求
│   └── utils                     // 常用工具类
├── components                 // 组件目录
├── layouts                    // 布局目录
├── middleware                 // 中间件目录
├── pages                      // 页面目录
├── plugins                    // 插件目录
├── static                     // 静态文件目录
├── store                      // 全局 store管理
├── .gitignore                 // git 忽略项
├── nuxt.config.js             // 配置文件
└── package.json               // package.json
```

>我租用的服务器性能不高，后台是PHP的，所以我的项目都是以静态文件发布的。
>nuxt的最优发布当然是动态发布，有需要的可以自行修改。
>如果觉得我的项目对你有用，请尽情使用，不收取版权费。稍微赞助一点点我可以升级下我的服务器！