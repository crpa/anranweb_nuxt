module.exports = {
    root: true,
    env: {
      browser: true,
      node: true
    },
    parserOptions: {
      parser: 'babel-eslint'
    },
    extends: [
      '@nuxtjs',
      'prettier',
      'prettier/vue',
      'plugin:prettier/recommended',
      'plugin:nuxt/recommended'
    ],
    plugins: [
      'prettier'
    ],
    // 自定义规则
    rules: {        //0，不启用这个规则   1，出现问题会有警告   2，出现问题会报错
      "vue/no-v-html":0,
      "no-useless-escape": 0
      }
  }